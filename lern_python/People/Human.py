class Human:
    """Some human."""
    instanceCount = 0

    def __init__(self, name, age):
        self.name = name
        self.age = age
        Human.instanceCount += 1

    def __str__(self):
        # return "Name: " + self.name + \
        #        "\nAge: " + str(self.age)
        return "Name: {0}, Age: {1}".format(
                    self.name, self.age)

    def get_name(self):
        return "Human: " + self.name
