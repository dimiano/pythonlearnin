from .Human import Human


class Student(Human):
    """Some smart human."""
    def __init__(
            self, name, age, uni, is_major=False):  # is_major is optional with default value
        super().__init__(name, age)
        self.uni = uni
        self.is_major = is_major

    def __str__(self):
        # return str(super()) + \
        #        "\nUni: " + self.uni + \
        #        "\nMajor: " + str(self.is_major)
        return "Name: {0}, Age: {1}, Uni: {2}, Major: {3}".format(
            self.name, self.age, self.uni, str(self.is_major))

    # @staticmethod # makes static
    def get_name(self):
        return "Student: " + self.name
