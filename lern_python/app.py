import People
# from .People.Student import Student
import random


# function
def pow_func(num, powr):
    if num < 0 and not (num > 100):
        return 0
    elif num == 1 or powr == 0 and powr != 100:
        return 1
    else:
        return num ** powr


# multiple comments
'''
in_tmp = input("Enter something: ")
res = pow_func(2, 32)
print("Result: " + str(res))
res = pow_func(0, 32)
print("Result: " + str(res))
res = pow_func(1, 32)
print("Result: " + str(res))
res = pow_func(1, 0)
print("Result: " + str(res))
'''

# dictionary
someDictionary = {
    "key1": "value1",
    "key2": "value2",
    "key3": "value3"
}


# while loop
def dict_function(key):
    i = 1
    while i <= 5:
        i += 1
    return someDictionary.get(key, "default value")


# tmp = dict_function("key1")
# print("Value: " + tmp)
# tmp = dict_function("no_key")
# print("Value: " + tmp)


# for loop
# try except
def for_function(string):
    res = ""
    try:
        for letter in string:
            if letter.lower() in "aeiou":  # wowels
                res += letter + ", "
            else:
                res += letter * 2 + ", "
    except ZeroDivisionError as err:
        res = "Zero"
        print(err)
    except ValueError:
        res = "Value"
    except Exception:  # catch all
        res = "Error"
    else:
        print('We are good')
    finally:
        print('Finally.')
# Именно в таком порядке:
    # try,
    # группа except,
    # затем else,
    # и только потом finally.

    return res

    # arr = [1, 2, 3, 4, 5]
    # arr = range(3, 10)
    # if arr:  # if 'res' is not empty
    #     for num in arr:
    #         res += str(num) + ", "
    # return res

    # names = ["Name1", "Name2", "Name3"]
    # for index in range(len(names)):
    #     res += names[index] + ", "
    # return res


# tmp = for_function("sex")[:-2]
# print(tmp)

# two dimensional array
arr_grid = [
    [1, 2, 3],
    [3, 4, 5],
    [5, 6, 7],
    [7, 8, 9],
    [0]
]
# print(arr_grid[1][2])
# for row in arr_grid:
#     print("- ", end="")
#     for col in row:
#         print(col, end=", ")


# modules usage
# read_file("file.txt")
# input()
# write_file("123.txt")
# input()
# append_file("123.txt")

tmp = random.randint(1, 10)
# print("Random :" + str(tmp))

# objects and classes
# stud1 = Student("Jim", 18, "Uni1")
# stud2 = Student("Jess", 22, "Uni1", True)
stud1 = People.Student("Jim", 18, "Uni1")
stud2 = People.Student("Jess", 22, "Uni1", True)
print(stud1)
print(stud2)

s = "abcdefg"
# print(s[-7:-2:2])
# print(s[:6:2])
#
# print(44 / 5)
# print(44 // 5)
# print(44 % 5)
# print(divmod(44, 5))

# string format
coord = {'latitude': '37.24N', 'longitude': '-115.81W'}
# print('Coordinates: {latitude}, {longitude}'.format(**coord))
# coord = (3, 5)
# print('X: {0[0]};  Y: {0[1]}'.format(coord))
# "repr() shows quotes: {!r}; str() doesn't: {!s}".format('test1', 'test2')
# "repr() shows quotes: 'test1'; str() doesn't: test2"
# print('{:<30}'.format('left aligned'))
# print('{:>30}'.format('right aligned'))
# print('{:^30}'.format('centered'))
# print('{:*^30}'.format('centered'))  # use '*' as a fill char
# print('{:+f}; {:+f}'.format(3.14, -3.14))  # show it always
# print('{: f}; {: f}'.format(3.14, -3.14))  # show a space for positive numbers
# print('{:-f}; {:-f}'.format(3.14, -3.14))  # show only the minus -- same as '{:f}; {:f}'
# # format also supports binary numbers
# print("int: {0:d};  hex: {0:x};  oct: {0:o};  bin: {0:b}".format(42))
# # with 0x, 0o, or 0b as prefix:
# print("int: {0:d};  hex: {0:#x};  oct: {0:#o};  bin: {0:#b}".format(42))
points = 19.5
total = 22
# print('Correct answers: {:.2%}'.format(points/total))

age = 20
name = 'Swaroop'
# print(f'{name} was {age} years old when he wrote this book')  # notice the 'f'
# print('{name} wrote {book}'.format(name='Swaroop', book='A Byte of Python')) # keyword-based

# list
lst = [c + d for c in 'list' if c != 'i' for d in 'spam' if d != 'a']
# print(lst)

# func = lambda x, y: x + y
# print(func(1, 2))
# print((lambda x, y: x + y)(1, 2))
# print((lambda *args: sum(args))(1, 2))

# lamda
double = lambda x: x * 2  # == def double(x): return x * 2

my_list = [1, 5, 4, 6, 8, 11, 3, 12]
new_list = list(filter(lambda x: (x % 2 == 0), my_list))  # lamda returns True/False
new_list = list(map(lambda x: x * 2 , my_list))  # lamda returns some value/object
# print(new_list)  # Output: [4, 6, 8, 12]
