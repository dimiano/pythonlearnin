import sys
from lern_python.decorators import *

# file read
encod = sys.getdefaultencoding()


@benchmark
@log
def read_file(path):
    """Open file and print its content."""
    print('Reading file: "{}"'.format(path), end='\n\n')
    # file = open(path, "r", encoding=encod)  # open Read
    # open(path, "r+") # open Read and Write
    # open(path, "w") # open Write
    # open(path, "a") # open Append (no change, append only)

    with open(path, "r", encoding=encod) as file:
        if file.readable():
            print(file.readline().strip())
            for line in file.readlines():
                print(line.strip())
            print()
    # file.close()  # no need using 'with'


def write_file(path):
    """Write a file.

    Keyword arguments:
    path -- the file path to write

    """
    file = open(path, "w", encoding=encod)
    if file.writable():
        file.writelines(["Some", "new", "information"])  # replace file's content


def append_file(path):
    file = open(path, "a", encoding=encod)
    if file.writable():
        file.write("\nSome another information")  # adds to the file's end


def get_file_ext(file):
    return file[file.index(".") + 1:]

