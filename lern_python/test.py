import sys
import unittest
from lern_python.People.Student import *


# @unittest.skip("showing class skipping")
class StudentCase(unittest.TestCase):
    def setUp(self):
        self.student = Student

    def tearDown(self):
        del self.student

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')

    def test_isupper(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    # @unittest.expectedFailure  # or decorator instead of 'assertRaises'
    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check whether s.split works, if separator isn't a string
        with self.assertRaises(TypeError):
            s.split(2)

    @unittest.skip("demonstrating skipping")
    def test_nothing(self):
        self.fail("shouldn't happen")

    @unittest.skipIf(__name__ != "something",
                     "not supported in this library version")
    def test_format(self):
        # Tests that work for only a certain version of the library.
        pass

    @unittest.skipUnless(sys.platform.startswith("win"), "requires Windows")
    def test_windows_support(self):
        # windows specific testing code
        pass


if __name__ == '__main__':
    unittest.main()
