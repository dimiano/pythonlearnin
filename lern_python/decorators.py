
def benchmark(func):
    """Log function's work time."""
    import time

    def wrapper(*arg, **kwarg):
        t = time.clock()
        res = func(*arg, **kwarg)
        print("Function: {}, Time:{}".format(func.__name__, time.clock() - t))
        return res
    return wrapper


def log(func):
    """Log function's name and arguments."""
    import functools  # now func.__name__ is not "wrapper", but real func name (in case multiple decorators are used)

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        res = func(*args, **kwargs)
        print("Function: {}, *args:{}, **kwargs:{}".format(func.__name__, args, kwargs))
        return res
    return wrapper


# # usage:
# @benchmark
# @log
# def foo(): print("something")
# # then just call foo() function

