#!/usr/bin/python3

import threading
import time

exitFlag = 0


class MyThread(threading.Thread):
    def __init__(self, thread_id, name, method_to_run):
        threading.Thread.__init__(self)
        self.threadID = thread_id
        self.name = name
        self.method = method_to_run


def run(self):
    print("Starting " + self.name)

    if self.method is not None:
        self.method()
    else:
        print_time(self.name, 500, 10)

    print("Exiting " + self.name)


def print_time(thread_name, delay, counter):
    while counter:
        if exitFlag:
            thread_name.exit()
        time.sleep(delay)
        print("%s: %s" % (thread_name, time.ctime(time.time())))
        counter -= 1


if __name__ == "__main__":
    # Create new threads
    thread2 = MyThread(2, "Thread-2", 2)
    thread1 = MyThread(1, "Thread-1", 1)

    # Start new Threads
    thread1.start()
    thread2.start()
    thread1.join()
    thread2.join()
    print("Exiting Main Thread")
