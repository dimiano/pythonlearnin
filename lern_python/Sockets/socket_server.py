#!/usr/bin/python            # This is server.py file

import socket                # Import socket module

s = socket.socket()          # Create a socket object
# s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = socket.gethostname()  # Get local machine name
port = 12121                 # Reserve a port for your service.
print(f'Socket server Bind: {host}:{port}')
s.bind((host, port))         # Bind to the port

print('Socket server listening...')
s.listen(1)                  # Now wait for client connection.
while True:
    c, addr = s.accept()     # Establish connection with client.
    print(f'Got connection from: {addr[0]}:{addr[1]}')
    c.send(b'Thank you for connecting')
    c.close()                # Close the connection
