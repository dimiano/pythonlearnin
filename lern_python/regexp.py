#!/usr/bin/python3
import re
import glob

counter = 0


def replace_file_content(file_path, find, replace):

    global counter
    res = None

    with open(file_path, "r+", encoding='utf-8') as file:
        s = file.read()
        # print("read:\n" + s)
        if re.search(find, s):
            res = re.sub(find, replace, s)
        else:
            pass  # print(f"File '{file_path}' is untouched")

    if res is not None:
        with open(file_path, "w+", encoding='utf-8') as wfile:
            wfile.write(res)
            print(f"File '{file_path}' updated")
            counter += 1


def add_ref(vbproj):

    find = r"(Bin\\Infor.Blending.Client.AppBase.dll</HintPath>[\s\S]*?</Reference>)"
    replace = r"""
    <Reference Include="Infor.Blending.Client.Interfaces, Version=1.0.0.0, Culture=neutral, processorArchitecture=MSIL">
      <SpecificVersion>False</SpecificVersion>
      <HintPath>..\\..\\..\\..\\Bin\\Infor.Blending.Client.Interfaces.dll</HintPath>
    </Reference>"""

    replace_file_content(vbproj, find, r"\1" + replace)


def add_ref_c(file_path):

    find = r"(Bin\\Infor.Blending.Client.Interfaces.dll</HintPath>[\s\S]*?</Reference>)"
    replace = r"""
    <Reference Include="Infor.Blending.Controls.CReportViewer, Version=8.1.1.0, Culture=neutral, processorArchitecture=x86">
      <SpecificVersion>False</SpecificVersion>
      <HintPath>..\\..\\..\\..\\Bin\\Infor.Blending.Controls.CReportViewer.dll</HintPath>
    </Reference>
    <Reference Include="Infor.Blending.Controls.CRulerGauge, Version=8.1.1.0, Culture=neutral, processorArchitecture=x86">
      <SpecificVersion>False</SpecificVersion>
      <HintPath>..\\..\\..\\..\\Bin\\Infor.Blending.Controls.CRulerGauge.dll</HintPath>
    </Reference>"""

    replace_file_content(file_path, find, r"\1" + replace)


def change_namespace(file_path):

    find = r"ST.Const(\w+)"
    replace = r"AT.Services.BService.Const"

    replace_file_content(file_path, find, replace + r"\1")


def find_in_files(file_path):

    find = r"AT.BApp.Lazyness"
    replace = r"AT.Lazyness"

    replace_file_content(file_path, find, replace)


def replace_in_files():

    exclude_path = "\\A\\"
    path = r"C:\\Projects\\TESTDT81\\Source\\Infor.Blending.Apps\\**\\*.vb"
    # path = r"C:\\Projects\\TESTDT81\\Source\\Infor.Blending.Apps\\**\\*.vbproj"
    # path = r"C:\\*.vb"
    fls = glob.glob(path, recursive=True)

    for f in fls:
        if exclude_path in f:
            print(f"Skipped: '{f}'")
            continue
        # print("found: " + f)
        find_in_files(f)  # add_ref_c(f)

    print("Total files processed: " + str(counter))
    print("Total files found: " + str(len(fls)))


if __name__ == "__main__":
    replace_in_files()
