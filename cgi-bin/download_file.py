#!/usr/bin/python

# HTTP Header
print('Content-Disposition: attachment; filename = "FileName.txt"\n\n')
print('Content-Type:application/octet-stream; name = "FileName.txt"\n')

# Actual File Content will go here.
with open("some_file.txt", "rb") as file:
    if file.readable():
        file_str = file.read()
        print("File content:")
        print(file_str)
