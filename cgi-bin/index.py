#!/usr/bin/env python3

# print("Set-cookie: some_name=value; expires=Wed May 18 03:33:20 2033; path=/cgi-bin/; httponly")
print("Set-cookie: name=value;")
print("Content-type: text/html")
print()
print("<title>Form data processing</title>")
print("<h1>Hello world!<br />Site is working...</h1>")
print("<p><a href='../web/pages/index.html'><h3>Main page</h3></a></p>")
print("<p><a href='wall.py'><h3>Wall page</h3></a>")
print("<p><a href='sql.py'><h3>Show users...</h3></a></p>")
