#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import os
import random
import sys
import time


class Wall:
    """Twitter-like user's wall."""

    USERS = 'cgi-bin/users.json'
    WALL = 'cgi-bin/wall.json'
    COOKIES = 'cgi-bin/cookies.json'
    enc = sys.getdefaultencoding()

    def __init__(self):
        """Create initial files if they are not created"""

        self._init_files(self.USERS)
        self._init_files(self.WALL, {"posts": []})
        self._init_files(self.COOKIES)

    def find(self, user, password=None, enc='utf-8'):
        """Check if user is registered"""

        with open(self.USERS, 'r', encoding=enc) as f:
            users = json.load(f)

        if user in users and (password is None or password == users[user]):
            return True

        return False

    def register(self, name, password, enc='utf-8'):
        """Register user, return true if created successfully"""

        if self.find(name):
            return False  # user already exists

        with open(self.USERS, 'r', encoding=enc) as f:
            users = json.load(f)

        users[name] = password

        with open(self.USERS, 'w', encoding=enc) as f:
            json.dump(users, f)

        return True

    def set_cookie(self, name, enc='utf-8'):
        """Write cookie to file, return created cookie"""

        with open(self.COOKIES, 'r', encoding=enc) as f:
            cookies = json.load(f)

        cookie = str(time.time()) + str(random.randrange(10**14))
        cookies[cookie] = name

        with open(self.COOKIES, 'w', encoding=enc) as f:
            json.dump(cookies, f)

        return cookie

    def find_cookie(self, cookie, enc='utf-8'):
        """FInd user name by cookie"""

        with open(self.COOKIES, 'r', encoding=enc) as f:
            cookies = json.load(f)

        res = cookies.get(cookie)
        return res

    def publish(self, user, text, enc='utf-8'):
        """Public text"""

        with open(self.WALL, 'r', encoding=enc) as f:
            wall = json.load(f)

        wall['posts'].append({'user': user, 'text': text})

        with open(self.WALL, 'w', encoding=enc) as f:
            json.dump(wall, f)

    def html_list(self, enc='utf-8'):
        """Post list to show on page"""

        with open(self.WALL, 'r', encoding=enc) as f:
            wall = json.load(f)

        posts = []
        for post in wall['posts']:
            content = f"{post['user']} : {post['text']}"
            posts.append(content)

        res = '<br>'.join(posts)
        return res

    @staticmethod
    def _init_files(file, data={}, enc='utf-8'):
        try:
            with open(file, 'r', encoding=enc):
                pass
        except FileNotFoundError:
            with open(file, 'w', encoding=enc) as f:
                json.dump(data, f)

    @staticmethod
    def get_cookies(cookies):
        cookies_dict = {}
        if cookies is not None:
            for cookie in cookies:
                cookie = cookie.split('=')
                cookies_dict[cookie[0]] = cookie[1]

        return cookies_dict
