#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cgi
import html
import http.cookies
import os

from _wall_repository import Wall  # 'Unresolved reference' is OK
wall = Wall()

cookie_dict = wall.get_cookies(os.environ.get("HTTP_COOKIE").split('; '))
cookie = http.cookies.SimpleCookie(cookie_dict)
session = cookie.get("session")
if session is not None:
    session = session.value
user = wall.find_cookie(session)  # find user by cookie provided

form = cgi.FieldStorage()
action = form.getfirst("action", "")

if action == "publish":
    text = form.getfirst("text", "")
    text = html.escape(text)
    if text and user is not None:
        wall.publish(user, text)
elif action == "login":
    login = form.getfirst("login", "")
    login = html.escape(login)
    password = form.getfirst("password", "")
    password = html.escape(password)
    if wall.find(login, password):
        cookie = wall.set_cookie(login)
        print(f'Set-cookie: session={cookie}')
    elif wall.find(login):
        pass  # TODO: show warning
    else:
        wall.register(login, password)
        cookie = wall.set_cookie(login)
        print(f'Set-cookie: session={cookie}')

pattern = '''
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>The Wall</title>
</head>
<body>    
    {login}

    {posts}

    {publish}
</body>
</html>
'''

if user is not None:
    login = ''
    pub = '''
    <form action="/cgi-bin/wall.py">
        <textarea name="text"></textarea>
        <input type="hidden" name="action" value="publish">
        <input type="submit" value="Publish">
    </form>
    '''
else:
    login = '''
    Login & registration form. When not existing user is provided it will be created then.
    <form action="/cgi-bin/wall.py">
        Login: <input type="text" name="login">
        Pass:  <input type="password" name="password">
        <input type="hidden" name="action" value="login">
        <input type="submit">
    </form>
    '''
    pub = ''

print('Content-type: text/html\n')
print(pattern.format(login=login, posts=wall.html_list(), publish=pub))
