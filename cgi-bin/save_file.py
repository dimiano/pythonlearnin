#!/usr/bin/python

import os
import cgi
import cgitb

cgitb.enable()

form = cgi.FieldStorage()

# Get filename here.
fileitem = form['filename']

# Test if the file was uploaded
if fileitem.filename:
    # strip leading path from file name to avoid
    # directory traversal attacks
    fn = os.path.basename(fileitem.filename)
    # fn = os.path.basename(fileitem.filename.replace("\\", "/")) # for Linux
    with open('/temp/' + fn, 'wb') as file:
        if file.writable():
            file.write(fileitem.file.read())

    message = 'The file "' + fn + '" was uploaded successfully'
else:
    message = 'No file was uploaded'

print(f"""\
Content-Type: text/html\n\n
<html>
<body>
   <p>{message}</p>
</body>
</html>""")
