#!/usr/bin/env python3

from os import environ
import cgi
import cgitb
import html
import http.cookies as ck

cgitb.enable()

form = cgi.FieldStorage()
print("Content-type: text/html\n")

# input
fname = form.getfirst("fname", "not set")
lname = form.getfirst("lname")

# checkbox
if form.getvalue('maths'):
    math_flag = "ON"
else:
    math_flag = "OFF"

if form.getvalue('physics'):
    physics_flag = "ON"
else:
    physics_flag = "OFF"

# radio
if form.getvalue('subject'):
    sex = form.getvalue('subject')
else:
    sex = "Not set"

# textarea
if form.getvalue('textcontent'):
    text_content = form.getvalue('textcontent')
else:
    text_content = "Not entered"

# dropdown
if form.getvalue('dropdown'):
    subject = form.getvalue('dropdown')
else:
    subject = "Not entered"

if not fname or not lname:
    print("""<!DOCTYPE HTML>
             <html><head>
                 <meta charset="utf-8">
                 <title>Error</title>
             </head>
             <body>""")
    print("<H1>Error</H1>")
    print("<p>Please fill in the first name and last name fields.<p>")
else:
    fname = html.escape(fname)
    lname = html.escape(lname)
    print("""<!DOCTYPE HTML>
            <html><head>
                <meta charset="utf-8">
                <title>Form data processing</title>
            </head>
            <body>""")

    print("<h1>Form data processing!</h1>")
    print(f"<p>First Name: <b>{fname}</b></p>")
    print(f"<p>Last Name: <b>{lname}</b></p><br />")
    print(f"<p>CheckBox Maths is: <b>{math_flag}</b></p>")
    print(f"<p>CheckBox Physics is: <b>{physics_flag}</b></p>")
    print(f"<p>Selected Sex is: <b>{sex}</b></p>")
    print(f"<p>Entered Text Content is: <b>{text_content}</b></p>")
    print(f"<p>Selected Subject is: <b>{subject}</b></p><br />")

# cookie processing
print(f"<p><b>Cookie</b>:</p>")

env_cookie = environ.get('HTTP_COOKIE')
if env_cookie is not None:
    i = 1
    for cookie in map(str.strip, str.split(env_cookie, ';')):
        (key, value) = str.split(cookie, '=')
        print(f"<p>{i}. {key}={value}</p>")
        i += 1

cookie = ck.SimpleCookie(env_cookie)
cookie_name = cookie.get("name")
if cookie_name is None:
    print("Set-cookie: some_name=some_value")
else:
    print(f"<p>_{cookie_name.key}={cookie_name.value}_</p>")
    # pass  # use cookie_name.value


print("</body>")
print("</html>")
