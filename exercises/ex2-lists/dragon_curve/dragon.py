#  Funkcja powinna zwrócić listę znaków 'L' i 'R' tworzoną wg. następujących reguł
#  dla n = 1 lista powinna mieć postać ['R']
#  dla n > 1 lista powinna składać się z listy dla n-1, dodanego znaku 'R' oraz 
#  listy dla n-1 obróconej przez funkcję flip (patrz dalszy opis)
#  Przykład:
#  dragon_curve(1) ----> ['R']
#  dragon_curve(2) ----> ['R', 'R', 'L']
#  dragon_curve(3) ----> ['R', 'R', 'L', 'R', 'R', 'L', 'L']
def dragon_curve(n):
    result = []
    return result


#  Funkcja powinna obracać podaną listę w następujący sposób
#  1. Lista podana jako argument powinna zostać odwrócona
#  2. Każdy znak 'R' powinien zostać zamieniony na 'L'
#     Każdy znak 'L' powinien zostać zamieniony na 'R'
def flip(step_list):
    return step_list
