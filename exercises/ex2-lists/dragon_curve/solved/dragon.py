def dragon_curve_r(n): 
    if n == 1:
        return ['R']
    else:
        current = dragon_curve_r(n - 1)
        flipped = flip(current[:])
        current.append('R')
        current.extend(flipped)
        return current

#  Funkcja powinna zwrócić listę znaków 'L' i 'R' tworzoną wg. następujących reguł
#  dla n = 1 lista powinna mieć postać ['R']
#  dla n > 1 lista powinna składać się z listy dla n-1, dodanego znaku 'R' oraz 
#  listy dla n-1 obróconej przez funkcję flip (patrz dalszy opis)
#  Przykład:
#  dragon_curve(1) ----> ['R']
#  dragon_curve(2) ----> ['R', 'R', 'L']
#  dragon_curve(3) ----> ['R', 'R', 'L', 'R', 'R', 'L', 'L']
def dragon_curve(n):
    result = []
    if n < 1:
        return result
    result.append('R')
    n -= 1
    for i in range(n):
        flipped = (flip(result[:]))
        result.append('R')
        result.extend(flipped)
    return result

#  Funkcja powinna obracać podaną listę w następujący sposób
#  1. Lista podana jako argument powinna zostać odwrócona
#  2. Każdy znak 'R' powinien zostać zamieniony na 'L'
#     Każdy znak 'L' powinien zostać zamieniony na 'R'
def flip(step_list):
    step_list.reverse()
    for i, step in enumerate(step_list):
        if step == 'R':
            step_list[i] = 'L'
        else:
            step_list[i] = 'R'
    return step_list
