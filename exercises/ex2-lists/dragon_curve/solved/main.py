import sys
from tkinter import *

from dragon import dragon_curve

SEGMENT_LENGTH = 5  # px

def current_direction(dx, dy, seg_len):
    if dy == -seg_len:
        current_dir = 'UP'
    elif dy == seg_len:
        current_dir = 'DOWN'
    elif dx == -seg_len:
        current_dir = 'LEFT'
    elif dx == seg_len:
        current_dir = 'RIGHT'
    return current_dir

def next_segment(previous_segment, next_dir, seg_len=5):
    next_seg_map = {
            'LEFT': {'L': lambda x,y: (x, y, x, y + seg_len),
                     'R': lambda x,y: (x, y, x, y - seg_len)},
            'RIGHT': {'L': lambda x,y: (x, y, x, y - seg_len),
                      'R': lambda x,y: (x, y, x, y + seg_len)},
            'UP': {'L': lambda x,y: (x, y, x - seg_len, y),
                   'R': lambda x,y: (x, y, x + seg_len, y)},
            'DOWN': {'L': lambda x,y: (x, y, x + seg_len, y),
                     'R': lambda x,y: (x, y, x - seg_len, y)},
    }
    dx = previous_segment[2] - previous_segment[0]
    dy = previous_segment[3] - previous_segment[1]
    current_dir = current_direction(dx, dy, seg_len)
    return next_seg_map[current_dir][next_dir](previous_segment[2], previous_segment[3])

if __name__ == '__main__':
    if len(sys.argv) > 1 and int(sys.argv[-1]) >= 1:
        level = int(sys.argv[-1])
    else:
        level = 1
    
    master = Tk()

    w = Canvas(master, width=800, height=600)
    w.pack()

    segment = (600, 300, 600, 300 - SEGMENT_LENGTH)
    w.create_line(*segment)
    prev_segment = segment
    for next_segment_dir in dragon_curve(level):
        current_segment = next_segment(prev_segment, next_segment_dir)
        w.create_line(*current_segment)
        prev_segment = current_segment

    mainloop()
