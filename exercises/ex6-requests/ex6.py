import requests


# Uzupełnij funkcję get_post_body tak, aby zwracała treść postu o
# zadanym id
# URL: http://jsonplaceholder.typicode.com/posts/
def get_post_body(post_id):
    return data['body']

# Uzupełnij funkcję get_commenters_data w taki sposób, aby
# zwracała listę krotek, zawierających imię i nazwisko komentującego
# oraz jego adres e-mail
# URL: http://jsonplaceholder.typicode.com/posts/<post_id>/comments
def get_commenters_data(post_id):
    pass


# Uzupełnij funkcję get_user_posts w taki sposób, aby zwracała listę
# tematów postów użytkownika o zadanym id
# URL: http://jsonplaceholder.typicode.com/posts?userId=<user_id>
def get_user_posts(user_id):
    pass

if __name__ == '__main__':
    print(get_post_body(10))
    print('=' * 20)
    print(get_commenters_data(10))
    print('=' * 20)
    print(get_user_posts(1))
