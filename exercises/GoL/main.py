import tkinter as tk
import tkinter.ttk as ttk

import game
from shapes import shapes

root = tk.Tk()
board_canvas = None

class GOLBoardCanvas(tk.Canvas):
    CELL_WIDTH = 10
    CELL_HEIGHT = 10
    CELL_FREE_COLOR = 'grey'
    CELL_TAKEN_COLOR = 'yellow'

    def __init__(self, cells_x, cells_y, *args, **kwargs):
        super(GOLBoardCanvas, self).__init__(*args, **kwargs)
        self.cells_x = cells_x
        self.cells_y = cells_y

    def repaint(self, board):
        y_pos = 0
        for cell_y in range(self.cells_y):
            x_pos = 0
            for cell_x in range(self.cells_x):
                color = self.get_cell_color(board, cell_x, cell_y)
                self.create_rectangle(x_pos, y_pos,
                                      x_pos + self.CELL_WIDTH, y_pos + self.CELL_HEIGHT,
                                      fill=color)
                x_pos += self.CELL_WIDTH
            y_pos += self.CELL_HEIGHT

    def get_cell_color(self, board, cell_x, cell_y):
        if board[cell_y][cell_x] == 1:
            color = self.CELL_TAKEN_COLOR
        else:
            color = self.CELL_FREE_COLOR
        return color

    def coords_to_cell(self, x, y):
        return x // self.CELL_WIDTH, y // self.CELL_HEIGHT

    def repaint_cell(self, cell_x, cell_y, board):
        x_pos = cell_x * self.CELL_WIDTH
        y_pos = cell_y * self.CELL_HEIGHT
        color = self.get_cell_color(board, cell_x, cell_y)
        self.create_rectangle(x_pos, y_pos,
                              x_pos + self.CELL_WIDTH, y_pos + self.CELL_HEIGHT,
                              fill=color)


def clicked(event):
    board_canvas = event.widget
    x, y = board_canvas.coords_to_cell(event.x, event.y)
    game.change_state(game.board_, x, y)
    board_canvas.repaint_cell(x, y, game.board_)

    print('Clicked at: {}, {}'.format(x, y))


def next_step():
    game.next_move(game.board_)
    board_canvas.repaint(game.board_)
    print('Next step')


def clear():
    game.clear_board(game.board_)
    board_canvas.repaint(game.board_)
    print('Clear')


def add_shape(shape_name):
    shape = shapes[shape_name]
    game.add_shape(game.board_, shape)
    board_canvas.repaint(game.board_)


def init_ui():
    root.geometry("1024x768+300+300")
    ctrl_frame = tk.Frame(root, bd=1)
    ctrl_frame.pack(side=tk.LEFT, fill=tk.Y)
    board_frame = tk.Frame(root)
    board_frame.pack(fill=tk.BOTH)
    global board_canvas
    board_canvas = GOLBoardCanvas(game.BOARD_WIDTH, game.BOARD_HEIGHT,
                                  board_frame, width=GOLBoardCanvas.CELL_WIDTH * game.BOARD_WIDTH,
                                  height=GOLBoardCanvas.CELL_HEIGHT * game.BOARD_HEIGHT)
    board_canvas.pack()
    board_canvas.repaint(game.board_)
    board_canvas.bind('<Button-1>', clicked)

    add_shape_frame = tk.Frame(ctrl_frame)
    add_shape_frame.pack(padx=5, pady=20)

    var = tk.StringVar()
    for shape_name in shapes:
        var.set(shape_name)
    shapes_options = tk.OptionMenu(add_shape_frame, var, *shapes.keys())
    shapes_options.pack(padx=5)

    add_shape_button = ttk.Button(add_shape_frame, text="Add", command=lambda: add_shape(var.get()))
    add_shape_button.pack(padx=5, pady=5)

    next_button = ttk.Button(ctrl_frame, text="Next", command=next_step)
    next_button.pack(padx=5, pady=5)
    # start_button = ttk.Button(ctrl_frame, text="Start")
    # start_button.pack(padx=5, pady=5)
    clear_button = ttk.Button(ctrl_frame, text="Clear", command=clear)
    clear_button.pack(padx=5, pady=5)

    storage_frame = tk.Frame(ctrl_frame)
    storage_frame.pack(padx=5, pady=20)
    load_button = ttk.Button(storage_frame, text="Load")
    load_button.pack(padx=5, pady=5)
    save_button = ttk.Button(storage_frame, text="Save")
    save_button.pack(padx=5, pady=5)


if __name__ == '__main__':
    game.init_board(game.board_)
    init_ui()
    root.mainloop()


