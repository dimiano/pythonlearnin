BOARD_WIDTH = 70
BOARD_HEIGHT = 70

# Plansza gry ułożona jako lista list 70x70
# 1 - komórka żywa
# 0 - komórka martwa
# [
#  [1, 0, 0, 1, 1],
#  [0, 1, 1, 0, 0],
#  [1, 1, 0, 0, 0],
#  [0, 0, 0, 0, 1]
# ]

# Zasady gry
# Martwa komórka, która ma dokładnie 3 żywych sąsiadów, staje się żywa w następnej jednostce czasu (rodzi się)
# Żywa komórka z 2 albo 3 żywymi sąsiadami pozostaje nadal żywa; przy innej liczbie sąsiadów umiera (z „samotności” albo „zatłoczenia”).

board_ = []


def init_board(board):
    del board[:]
    row = []
    for _ in range(BOARD_WIDTH):
        row.append(0)
    for i in range(BOARD_HEIGHT):
        board.append(row[:])


def change_state(board, x, y):
    """
    Zmienia wartość komórki o zadanych współrzędnych na przeciwną
    """
    pass


def clear_board(board):
    """
    Wstawia 0 w każdą komórkę
    """
    init_board(board)


def next_move(board):
    """
    Modyfikuje zawartość komórek planszy zgodnie z zasadami gry
    """
    pass


def get_neighbours_num(board, x, y):
    """
    Zwraca ilość sąsiadów żywych dla danej komórki
    """
    pass


def add_shape(board, shape):
    y = len(board) // 2
    for shape_row in shape:
        x = len(board[0]) // 2
        for shape_cell in shape_row:
            board[y][x] = shape_cell
            x += 1
        y += 1


def save_board(board, path):
    pass


def load_board(board, path):
    pass
