BOARD_WIDTH = 70
BOARD_HEIGHT = 70

board_ = []


def init_board(board):
    del board[:]
    row = []
    for _ in range(BOARD_WIDTH):
        row.append(0)
    for i in range(BOARD_HEIGHT):
        board.append(row[:])


def change_state(board, x, y):
    if board[y][x]:
        board[y][x] = 0
    else:
        board[y][x] = 1


def clear_board(board):
    init_board(board)


def next_move(board):
    changes = {}
    for y in range(len(board)):
        for x in range(len(board[y])):
            neighbours_num = get_neighbours_num(board, x, y)
            if board[y][x] == 1:
                if neighbours_num == 1 or neighbours_num == 0:
                    changes[(x, y)] = 0
                elif neighbours_num >= 4:
                    changes[(x, y)] = 0
            elif board[y][x] == 0 and neighbours_num == 3:
                changes[(x, y)] = 1
    for coords, value in changes.items():
        board[coords[1]][coords[0]] = value


def get_neighbours_num(board, x, y):
    neighbours_num = 0
    if x < (len(board[y]) - 1):
        neighbours_num += board[y][x + 1]
        if y < (len(board) - 1):
            neighbours_num += board[y + 1][x + 1]
        if y > 0:
            neighbours_num += board[y - 1][x + 1]
    if x > 0:
        neighbours_num += board[y][x - 1]
        if y < (len(board) - 1):
            neighbours_num += board[y + 1][x - 1]
        if y > 0:
            neighbours_num += board[y - 1][x - 1]

    if y < (len(board) - 1):
        neighbours_num += board[y + 1][x]
    if y > 0:
        neighbours_num += board[y - 1][x]
    return neighbours_num


def add_shape(board, shape):
    y = len(board) // 2
    for shape_row in shape:
        x = len(board[0]) // 2
        for shape_cell in shape_row:
            board[y][x] = shape_cell
            x += 1
        y += 1


def save_board(board, path):
    pass


def load_board(board, path):
    pass
