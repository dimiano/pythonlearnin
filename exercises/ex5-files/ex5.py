import string
PICTURES = ['scrambled_pic1.jpg', 'scrambled_pic2.jpg', 'scrambled_pic3.jpg']
TEXT_FILE = 'text_sample.txt' 

# Uzupełnij funkcję word_stats w taki sposób, aby zwracała
# ilość wystąpień słów z listy words w pliku o nazwie przekazanej 
# w parametrze filename
# Wynikiem powinien być słownik o nas†epującej strukturze:
# {'słowo1': ilość wystąpień, 'słowo2': ilość_wystąpień}
def word_stats(filename, words):
    """
    Args:
        filename: nazwa pliku
        words: lista słów do wyszukiwania
    Returns:
        dict
    """
    stats = {}
    return stats


# 1) Uzupełnij funkcję word_stats_all w taki sposób, aby zwracała
# ilość wystąpień słów w pliku o nazwie przekazanej 
# w parametrze filename
# Wynikiem powinien być słownik o nas†epującej strukturze:
# {'słowo1': ilość wystąpień, 'słowo2': ilość_wystąpień}
#
# 2) Po wykonaniu punktu 1) zmień funkcję tak, aby porównywała słowa niezależnie
# od wielkości liter. Funkcja powinna też zadbać o usunięcie znaków przestankowych z
# końcówek wyrazów.
def word_stats_all(filename):
    """
    Args:
        filename: nazwa pliku
    Returns:
        dict: statystyki wyrazów
    """
    stats = {}
    return stats


# Uzupełnij funkcję unscramble tak, aby zmieniła strukturę plików
# binarnych z listy filenames wg. następującego algorytmu:
# 1. Odczytać 100 bajtów pliku.
# 2. Jeśli udało się odczytać 100 bajtów to do pliku wyjściowego zapisujemy 
#   najpierw ostatnie 50 bajtów z odczytanego bufora, następnie pierwsze 50 bajtów.
# 3. Jeśli odczytaliśmy mniej niż 100 bajtów (nie ma więcej danych w pliku) to
#   zapisujemy do pliku wyjściowego odczytane dane bez zmian.
# punkty 1-3 należy powtórzyć dla całego pliku.
# 
# Funkcja nie powinna zwracać żadnej wartości, ale powinna tworzyć plik wyjściowy, 
# dla każdego pliku wejściowego. Nazwa pliku wyjściowego powinna być taka sama jak wejściowego
# z usuniętym prefiksem "scrambled_"
#
# Pliki wyjściowe powinny być obrazami w formacie JPG. Na którym z nich znajduje się zwierzę?
def unscramble(filenames):
    pass

if __name__ == '__main__':
    print('Statystyki dla wyrazów "the" and "whose"')
    print(word_stats(TEXT_FILE, ['the', 'whose']))
    print('=' * 20)
    print('Statystyki wystąpień dla wszystkich słów')
    print(word_stats_all(TEXT_FILE))
    print('=' * 20)
    print('Zmiana struktury plików...')
    unscramble(PICTURES)
    print('=' * 20)
