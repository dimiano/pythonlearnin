import string

def extract_words(filename):
        with open(filename, 'r') as f:
            for line in f:
                line = line.strip()
                line_words = line.split(' ')
                for line_word in line_words:
                    if len(line_word) == 0:
                        continue
                    if line_word[-1] in string.punctuation:
                        line_word = line_word[:-1]
                    line_word = line_word.strip()
                    yield line_word
                yield '\n'

if __name__ == '__main__':
    outfile = 'text_simplified.txt'
    with open(outfile, 'w') as g:
        for word in extract_words('text_sample.txt'):
            g.write(word + ' ')
