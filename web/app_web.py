import server
import webbrowser
import lern_python.threads as threads


def run_web_server():
    server.init()
    webbrowser.open(f"http://{server.default_page}")
    server.start_server()


thread = threads.MyThread(1, "Thread-1", run_web_server)
thread.start()
thread.join()
