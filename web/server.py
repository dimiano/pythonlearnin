from http.server import *

address = ""
default_page = ""
server_address = ()


def init(name="", port=5555):
    """Initialize HTTP server with provided address and port."""

    global address
    global default_page
    global server_address

    server_address = (name, port)
    # address = "{}:{}".format(name if name is not None else "localhost", port)
    address = "{}:{}".format(name or 'localhost', port)
    default_page = f"{address}/cgi-bin/index.py"


def start_server():
    """Starts HTTP server with provided address and port."""

    httpd = HTTPServer(server_address, CGIHTTPRequestHandler)
    print(f'Server running at: {address}')
    httpd.serve_forever()
    print('\nServer stopped\n')
